from flask import *
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
import base64
from datetime import datetime


app = Flask(__name__)
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'
app.config['SQLALCHEMY_DATABASE_URI']='mysql+pymysql://root:1234@localhost/crud'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
migrate = Migrate(app,db)

class content(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    email = db.Column(db.String(20), unique = True, nullable=False)
    password = db.Column(db.String(20),nullable = False)
    first_name = db.Column(db.String(100),nullable=False)
    last_name = db.Column(db.String(100),nullable=False)
    company = db.Column(db.String(100),nullable=False)
    department = db.Column(db.Integer, db.ForeignKey('department.id'),nullable=False)
    role = db.Column(db.Integer, db.ForeignKey('role.id'),nullable=False)
    created_date =db.Column(db.String(20))
    update_date = db.Column(db.String(20))
    is_deleted = db.Column(db.Integer)

    def __init__(self,username,password,first_name,last_name,company,department,role):
        self.email = username
        self.password = password
        self.first_name = first_name
        self.last_name = last_name
        self.company = company
        self.department = department
        self.role = role
        self.created_date = str(datetime.now())
        self.update_date = str(datetime.now())
        self.is_deleted = 0

class department(db.Model):
    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String(50),unique=True,nullable=True)
    user_id = db.relationship('content', lazy=True)

    def __init__(self,id,name):
        self.id = id
        self.name = name

class role(db.Model):
    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String(50),unique=True,nullable=True)
    t_id = db.relationship('content', lazy=True)

    def __init__(self,id,name):
        self.id = id
        self.name = name

@app.route('/')
def login():
    try:
        return render_template("Login.html",msg="")
    except Exception as e:
        flash("An Error occured")
        return render_template('Login.html',error=str(e))
        

@app.route('/rr')
def rr():
    try:
        return render_template("Registration.html")
    except Exception as e:
        flash("An Error occured")
        return render_template('Login.html',error=str(e))
        



@app.route('/register',methods=['POST'])
def register():
    try:
        username = request.form['email']
        password = request.form['psw']
        r_password = request.form['psw-repeat']
        first_name = request.form['first']
        last_name= request.form['last']
        company = request.form['company']
        dept = department.query.filter_by(name=request.form['dept']).first()
        r = role.query.filter_by(name=request.form['role']).first()
        print username,password,first_name,last_name,company,dept.id,r.id
        if password == r_password:
            password = base64.b64encode(r_password)
            user = content(username,password,first_name,last_name,company,dept.id,r.id)
            db.session.add(user)
            db.session.commit()
            return redirect(url_for('home'))
        else:
            flash("An Error occured")
            return render_template('Login.html',error="Password did not match")       
    except Exception as e:
        flash("An Error occured")
        return render_template('Login.html',error="Email is already used")
        

@app.route('/home')
def home():
    try:
        result = content.query.all()
        roles = role.query.all()
        dept = department.query.all()
        if result == []:
            flash("An Error occured")
            return render_template('Login.html',error="User Not Found")
        else:
            return render_template('home.html',books=result,role = roles,dept=dept)
    except Exception as e:
        flash("An Error occured")
        return render_template('Login.html',error=str(e))
        

@app.route('/update/')
def update():
    try:
        email = request.args.get("email")
        user = content.query.filter_by(email=email).first()
        roles = role.query.filter_by(id=user.role).first()
        dept = department.query.filter_by(id=user.department).first()
        print user.company
        return render_template('update.html',user=user,role= roles.name,dept= dept.name)    
    except Exception as e:
        flash("An Error occured")
        return render_template('Login.html',error="Email is already used")
        

@app.route('/edit',methods=['POST'])
def edit():
    try:
        id = request.form['id']
        username = request.form['email']
        first_name = request.form['first']
        last_name= request.form['last']
        company = request.form['company']
        dept = department.query.filter_by(name=request.form['dept']).first()
        r = role.query.filter_by(name=request.form['role']).first()
        user = content.query.filter_by(id=id).first()
        user.email=username
        user.first_name=first_name
        user.last_name=last_name
        user.company=company
        user.department=dept.id
        user.role=r.id
        user.update_date = str(datetime.now())
        db.session.commit() 
        return redirect(url_for('home'))
    except Exception as e:
        flash("An Error occured")
        return render_template('Login.html',error="Email is already used")
        



@app.route('/delete/')
def delete():
    try:
        email = request.args.get("email")
        print email,2
        user = content.query.filter_by(email=email).first()
        user.is_deleted=1
        db.session.commit()
        return redirect(url_for('home'))
    except Exception as e:
        flash("An Error occured")
        return render_template('Login.html',error="Unable to grant request")
        

@app.route('/activate/')
def activate():
    try:
        email = request.args.get("email")
        print email,2
        user = content.query.filter_by(email=email).first()
        user.is_deleted=0
        db.session.commit()
        return redirect(url_for('home'))
    except Exception as e:
        flash("An Error occured")
        return render_template('Login.html',error="Unable to make grant request")
        

@app.route('/con_del')
def con_del():
    try:
        user = content.query.filter_by(is_deleted=1).delete()
        db.session.commit()
        return "All Inactive Record Deleted"
    except Exception as e:
        flash("An Error occured")
        return render_template('Login.html',error="Unable to remove data")
        

"""@app.route('/add_dept/<name>')
def add_dept(name):
    dept = department(name)
    db.session.add(dept)
    db.session.commit()


@app.route('/add_role/<name>')
def add_role(name):
    dept = role(name)
    db.session.add(dept)
    db.session.commit()
"""
if __name__=='__main__':
    app.run(debug=True)