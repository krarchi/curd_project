"""initial migration

Revision ID: cdaa17747dd1
Revises: 
Create Date: 2020-01-29 12:01:13.655000

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = 'cdaa17747dd1'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('content', 'department',
               existing_type=mysql.INTEGER(display_width=11),
               nullable=False)
    op.alter_column('content', 'role',
               existing_type=mysql.INTEGER(display_width=11),
               nullable=False)
    op.create_foreign_key(None, 'content', 'department', ['department'], ['id'])
    op.create_foreign_key(None, 'content', 'role', ['role'], ['id'])
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'content', type_='foreignkey')
    op.drop_constraint(None, 'content', type_='foreignkey')
    op.alter_column('content', 'role',
               existing_type=mysql.INTEGER(display_width=11),
               nullable=True)
    op.alter_column('content', 'department',
               existing_type=mysql.INTEGER(display_width=11),
               nullable=True)
    # ### end Alembic commands ###
